let axios = require('axios');
let cheerio = require('cheerio');
let fs = require('fs');
let auth = require('./auth-info');

const userName = auth.userName;
const password = auth.password;
const sessionURL = auth.sessionURL;

authURL = sessionURL + '/authenticate';
pageURL = sessionURL + '/landing.html?'

redirectAction = 'homeActionId';
requestCookie = '';

customerList = [];

Start();

function Start() {
    InitiateHandshake();

    AuthenticateSession().then(function (result) {
        RetrieveAdditionalResults().then(function (result) {
            // ViewStoredResults();
            SyncWithDatabase();
        });
    });


}

function InitiateHandshake() {
    // Axios GET request to the server URL
    axios({
        url: sessionURL,
        method: 'get'
    })
        .then(function (response) {

            // Get the cookie sent by the response from the server
            cookie = response['request']['res']['headers']['set-cookie'];

            // There are two headers for the cookie sent, iterate through each
            for (i = 0; i < cookie.length; i++) {
                // Truncate each cookie header to only the segment needed and store it for future requests
                requestCookie += cookie[i].substring(0, cookie[i].indexOf(';') + 1);
            }
        })
        .catch(function (error) {
            console.log(error);
        });
}


function AuthenticateSession() {
    return new Promise(function (resolve, reject) {
        axios({
            url: authURL,
            method: 'post',
            headers: {
                'Cookie': requestCookie
            },
            params: {
                'userName': userName,
                'password': password,
                'Submit': 'Submit',
                'redirectAction': redirectAction
            }
        })
            .then(function (response) {
                ParseResponseData(response)
                    .then(function (result) {
                        resolve();
                    })
            });
    });
}


function RetrieveAdditionalResults() {
    return new Promise(function (resolve, reject) {
        axiosPromises = [];
        prdPromises = [];

        for (i = 2; i <= 15; i++) {
            axiosPromise =
                axios({
                    url: pageURL,
                    method: 'get',
                    headers: {
                        'Cookie': requestCookie
                    },
                    params: {
                        'index': i
                    }
                });
            axiosPromises.push(axiosPromise);
        }
        Promise.all(axiosPromises)
            .then(function (unparsedResponses) {
                for (j = 0; j < unparsedResponses.length; j++) {
                    promise = ParseResponseData(unparsedResponses[j]);
                    prdPromises.push(promise);
                }
                Promise.all(prdPromises)
                    .then(function (parsedResponsesArr) {
                    });
                resolve();
            });
    });
}

function ParseResponseData(response) {
    return new Promise(function (resolve, reject) {
        const html = response.data;

        customerName = '';
        customerType = '';
        customerSoftware = '';
        $ = cheerio.load(html);

        $('.lfr-template').remove();


        $('.results-row').each(function (i, elem) {
            $(this).find('td').each(function (i, elem) {
                if ($(this).hasClass('col-1')) {
                    customerName = $(this).text();
                } else if ($(this).hasClass('col-2')) {
                    customerType = $(this).text();
                } else if ($(this).hasClass('col-3')) {
                    customerSoftware = $(this).text();
                }
            })
            customerList.push({ customerName, customerType, customerSoftware });
        });
        resolve();
    });
}

function SyncWithDatabase() {
    console.log('Here we would sync everything to the database');
}

function ViewStoredResults() {
    for (i = 0; i < customerList.length; i++) {
        console.log(customerList[i]['customerName'] + customerList[i]['customerSoftware']);
    }
}